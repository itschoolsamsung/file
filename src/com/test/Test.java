package com.test;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        File f = new File("test.txt");
        if (f.exists()) {
            System.out.println("exists");
        } else {
            System.out.println("not exists");
        }

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(f));
            writer.print("test\nanother test");
            writer.print(123);
            writer.println(456);
            writer.close();
        } catch (Exception e) {
            System.out.println("write error");
        }

        try {
            Scanner scanner = new Scanner(f);
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
            scanner.close();
        } catch (Exception e) {
            System.out.println("read error");
        }
    }
}
